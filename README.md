# FORM INPUT TUTORIAL

## Part 1 (User Interface)
Part 1 of the tutorial can be downloaded here<https://drive.google.com/file/d/1M-K9osMfFR8CM3Qej_4NnVFfSxWK1LrC/view?usp=sharing>

## Part 2 (Business Logic)
Part 2 of the tutorial can be downloaded here<https://drive.google.com/file/d/1v7frwrR-gKUxNjAeWwl7IsPIHFHXkrWo/view?usp=sharing>