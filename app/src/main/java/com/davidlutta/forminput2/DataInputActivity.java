package com.davidlutta.forminput2;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class DataInputActivity extends AppCompatActivity {
    EditText nameEditText;
    DatePicker DOBDatePicker;
    EditText emailEditText;
    CheckBox TCCheckbox;
    Spinner spinner;
    String[] status = {"full-time", "part-time", "student", "other"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Function that runs when the activity is being created
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_input);

        nameEditText = (EditText) findViewById(R.id.nameEditText);
        DOBDatePicker = (DatePicker) findViewById(R.id.DOBDatePicker);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        TCCheckbox = (CheckBox) findViewById(R.id.TCCheckbox);

        // take all elements from status and populate them inside the spinner
        spinner = (Spinner) findViewById(R.id.spinner);
        // Context this, DataInputActivity.this
//        ArrayAdapter<String> items = new ArrayAdapter<String>(DataInputActivity.this, R.layout.spinner_list_item, status);
//        spinner.setAdapter(items);
        spinner.setAdapter(new ArrayAdapter<String>(DataInputActivity.this, R.layout.spinner_list_item, status));
    }

    private void displayNextAlert() {
        String name = nameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String month = String.valueOf(DOBDatePicker.getMonth());
        String day = String.valueOf(DOBDatePicker.getDayOfMonth());
        String year = String.valueOf(DOBDatePicker.getYear());
        String date = String.format("%s %s %s", month, day, year);

        int selectedItemPosition = spinner.getSelectedItemPosition();
        String selectedStatus = status[selectedItemPosition];
        boolean checkBoxValue = TCCheckbox.isChecked();
        if (checkBoxValue) {
            //true
            new AlertDialog.Builder(DataInputActivity.this).setTitle("Details Entered")
                    .setMessage(String.format("Details Entered\n%s\n%s\n%s\n%s", name, email, date, selectedStatus))
                    .setNeutralButton("Back", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Do nothing here
                        }
                    }).show();
        } else {
            //false
            popupToast("Sorry, you must agree the terms even though you don't know what they are!!");
        }
    }

    public void nextButton(View view) {
        displayNextAlert();
    }

    private void popupToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_data_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemNext: // case where next is selected
                displayNextAlert();
                return true;
            case R.id.itemExit: // case where exit is selected
                popupToast("You want to exit but why not just start using another application");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}